# name:ck
# Time: 下午4:03
# File: 5.request_cookies.py
# coding:utf8
import requests

url = 'https://gitee.com/ckit/dashboard'

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36',
}

temp = 'user_locale=zh-CN; oschina_new_user=false; tz=Asia%2FShanghai; Hm_lvt_24f17767262929947cc3631f99bfd274=1568262431,1568263152,1568275452,1568620248; gitee-session-n=BAh7B0kiD3Nlc3Npb25faWQGOgZFVEkiJWQxMTE5Y2ZiNzM1ZDU2ZTRjN2VhYmY3ZGZmMzYwNTcyBjsAVEkiEF9jc3JmX3Rva2VuBjsARkkiMU9iejR6QlE5UE9ZOVI4bmxJZ215OXZCU1duVnpCUXFZNDF4RHM5RWw2dm89BjsARg%3D%3D--f8f0b4c58d6c3ef9ece48a5bc8c34d26e34affff; Hm_lpvt_24f17767262929947cc3631f99bfd274=1568620309'
cookie_list = temp.split('; ')

cookies = {cookie.split('=')[0]:cookie.split('=')[-1] for cookie in cookie_list}

# cookie_list = temp.split('; ')
# cookies = {}
#
# for cookie in cookie_list:
#     cookies[cookie.split('=')[0]] = cookie.split('=')[-1]
# print(cookies)

response = requests.get(url, headers=headers, cookies=cookies)

with open('mayun_cook3.html', 'wb')as f:
    f.write(response.content)
