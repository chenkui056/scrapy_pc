# name:ck
# Time: 下午6:36
# File: 10.request_jianshan_post.py
# coding:utf8

import requests
import json


class King():

    def __init__(self, word):
        self.url = 'http://fy.iciba.com/ajax.php?a=fy'

        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36',
            # 'Cookie':'iciba_u_rand=dc4421196b362650c6d3f225dd44e771%4027.155.93.86; iciba_u_rand_t=1568631031; UM_distinctid=16d39b20ecb7a9-006b4fb451295c-5373e62-144000-16d39b20ecc8e0; CNZZDATA1256573702=1574818008-1568629237-http%253A%252F%252Fwww.iciba.com%252F%7C1568629237; __gads=ID=b24b048522b1df9d:T=1568631049:S=ALNI_MYpJu4saM7F4NTtI-RAtgt5aKefzQ',
            # 'Host': 'fy.iciba.com',
            # 'Origin': 'http://fy.iciba.com/',
            # 'Referer': 'http://fy.iciba.com',
            # 'X-Requested-With': 'XMLHttpRequest',
            # 'Accept': 'application / json, text / javascript, * / *; q = 0.01',
            # 'Accept - Encoding': 'gzip, deflate',
            # 'Accept - Language': 'zh - CN, zh;q = 0.9, en - US;q = 0.8, en;q = 0.7',
            # 'Connection': 'keep - alive',


        }
        self.data = {
            'f': 'auto',
            't': 'auto',
            'w': word
        }

    def post_data(self):
        response = requests.post(self.url,  data=self.data,headers=self.headers)
        return response.content

    def parse_data(self,data):
        # loads 方法将 json 字符串 转换成Python字典
        dict_data = json.loads(data)

        print(dict_data['content']['out'])


    def run(self):
        response = self.post_data()
        # print(response)
        self.parse_data(response)

if __name__ == '__main__':

    word = input('输入要翻译的单词：')
    king = King(word)
    king.run()

