# name:ck
# Time: 下午3:46
# File: 4.request_mayun.py
# coding:utf8
import requests

url = 'https://gitee.com/ckit/dashboard'

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36',
    'Cookie': 'user_locale=zh-CN; oschina_new_user=false; tz=Asia%2FShanghai; Hm_lvt_24f17767262929947cc3631f99bfd274=1568262431,1568263152,1568275452,1568620248; gitee-session-n=BAh7B0kiD3Nlc3Npb25faWQGOgZFVEkiJWQxMTE5Y2ZiNzM1ZDU2ZTRjN2VhYmY3ZGZmMzYwNTcyBjsAVEkiEF9jc3JmX3Rva2VuBjsARkkiMU9iejR6QlE5UE9ZOVI4bmxJZ215OXZCU1duVnpCUXFZNDF4RHM5RWw2dm89BjsARg%3D%3D--f8f0b4c58d6c3ef9ece48a5bc8c34d26e34affff; Hm_lpvt_24f17767262929947cc3631f99bfd274=1568620309'

}

response = requests.get(url, headers=headers)

with open('mayun.html', 'wb') as f:
    f.write(response.content)
