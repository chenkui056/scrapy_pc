# name:ck
# Time: 下午2:45
# File: 11.request_session_sigoin_mayun.py
# coding:utf8
import requests
import re


def login():
    # session
    session = requests.session()

    # headers
    session.headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'

    }

    # url_1 获取token
    url_1 = 'https://gitee.com/login'

    # 发送请求，获取响应
    response_1 = session.get(url_1).content.decode()

    # 正则提取
    token = re.findall('name="authenticity_token" type="hidden" value="(.*?)" />', response_1)[0]
    print(token)

    # url_2登录
    url_2 = 'https://gitee.com/login'
    # 构建表单数据
    data = {
        'utf8': '✓',
        'authenticity_token': token,
        'user[login]': 'ckit',
        'user[password]': 'chenkui.0',
        'user[remember_me]': '0',
    }
    # 发送登录请求
    session.post(url_2, data=data)

    # url_3 验证
    url_3 = 'https://gitee.com/ckit/dashboard'

    response_3 = session.get(url_3)
    with open('mayun.html', 'w')as f:
        f.write(response_3.content.decode())


if __name__ == '__main__':
    login()
