# name:ck
# Time: 下午4:38
# File: 6.request_cookiejar_utils.py
# coding:utf8
import requests

url = 'https://www.baidu.com'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36',
}
response = requests.get(url,headers =headers)

print(response.cookies)

dict_cookies = requests.utils.dict_from_cookiejar(response.cookies)
print('cook:',dict_cookies)
#
# jar_cookies = requests.utils.cookiejar_from_dict(dict_cookies)
# print(jar_cookies)

# temp = 'user_locale=zh-CN; oschina_new_user=false; tz=Asia%2FShanghai; Hm_lvt_24f17767262929947cc3631f99bfd274=1568262431,1568263152,1568275452,1568620248; gitee-session-n=BAh7B0kiD3Nlc3Npb25faWQGOgZFVEkiJWQxMTE5Y2ZiNzM1ZDU2ZTRjN2VhYmY3ZGZmMzYwNTcyBjsAVEkiEF9jc3JmX3Rva2VuBjsARkkiMU9iejR6QlE5UE9ZOVI4bmxJZ215OXZCU1duVnpCUXFZNDF4RHM5RWw2dm89BjsARg%3D%3D--f8f0b4c58d6c3ef9ece48a5bc8c34d26e34affff; Hm_lpvt_24f17767262929947cc3631f99bfd274=1568620309'
# dict_cookies = requests.utils.dict_from_cookiejar(temp)
# print(dict_cookies)
